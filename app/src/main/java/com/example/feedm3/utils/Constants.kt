package com.example.feedm3.utils

object Constants {
    const val KEY_HEADER = "header"
    const val KEY_TITLE = "title"
    const val KEY_TITLE_BACKGROUND = "title_background"
    const val KEY_UP = "up"
    const val KEY_AVATAR = "avatar"
    const val KEY_EXCERPT = "excerpt"
}