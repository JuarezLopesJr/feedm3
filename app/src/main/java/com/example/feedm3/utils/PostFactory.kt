package com.example.feedm3.utils

import com.example.feedm3.data.Post

object PostFactory {
    fun makePosts(): List<Post> {
        return listOf(
            Post(
                "1",
                "Chapolin",
                "chapolin",
                "http://picsum.photos/800/400",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
            Post(
                "2",
                "Quase Nada",
                "quasenada",
                "http://picsum.photos/800/600",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
            Post(
                "3",
                "Dona Clotilde",
                "donaclotilde",
                "http://picsum.photos/900/400",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
            Post(
                "4",
                "Dona Florinda",
                "donaflorinda",
                "http://picsum.photos/600/400",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
            Post(
                "5",
                "Seu Madruga",
                "seumadruga",
                "http://picsum.photos/500/500",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
            Post(
                "6",
                "Jaiminho",
                "jaiminho",
                "http://picsum.photos/700/600",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
            Post(
                "7",
                "Professor Girafales",
                "professorgirafales",
                "http://picsum.photos/800/700",
                "Mussum Ipsum, cacilds vidis litro abertis. Aenean aliquam molestie leo, vitae iaculis nisl.Quem num gosta di mé, boa gentis num é.Sapien in monti palavris qui num significa nadis i pareci latim.Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose.",
                "27/03/2022",
            ),
        )
    }
}