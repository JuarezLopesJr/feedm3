package com.example.feedm3.ui.screens.blog

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.feedm3.R
import com.example.feedm3.data.Post
import com.example.feedm3.ui.screens.post.PostItem
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph

@RootNavGraph(start = true)
@Destination
@Composable
fun Blog(
    modifier: Modifier = Modifier,
    posts: List<Post>,
    onPostSelected: (Int) -> Unit
) {

    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        itemsIndexed(
            items = posts,
            key = { _, item -> item.id }
        ) { index, post ->
            val clickDescription = stringResource(id = R.string.cd_read_post, post.title)

            PostItem(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClickLabel = clickDescription) {
                        onPostSelected(index)
                    },
                post = post
            )
        }
    }
}