package com.example.feedm3.ui.screens.post

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.feedm3.R
import com.example.feedm3.data.Post
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph

@RootNavGraph
@Destination
@Composable
fun PostDetail(
    modifier: Modifier = Modifier,
    post: Post,
    handleNavigateUp: () -> Unit
) {
    val scrollState = rememberScrollState()

    Column(modifier = modifier) {
        AsyncImage(
            modifier = Modifier.fillMaxWidth(),
            model = post.image,
            contentScale = ContentScale.Crop,
            contentDescription = null,
        )

        Box(
            modifier = Modifier
                .background(Color.Black.copy(alpha = 0.4f))
                .fillMaxWidth()
        )

        Text(
            modifier = Modifier.padding(vertical = 16.dp),
            text = post.title,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp
        )

        IconButton(
            onClick = handleNavigateUp
        ) {
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = stringResource(id = R.string.cd_action_up),
                tint = Color.White
            )
        }
        Icon(
            modifier = Modifier
                .size(40.dp),
            imageVector = Icons.Default.AccountCircle,
            contentDescription = null
        )

        Text(
            modifier = Modifier.width(200.dp),
            text = post.author,
            textAlign = TextAlign.Center
        )

        Text(
            modifier = Modifier
                .verticalScroll(scrollState)
                .padding(start = 16.dp, end = 16.dp, bottom = 78.dp),
            text = post.excerpt
        )
    }
}