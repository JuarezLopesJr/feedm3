package com.example.feedm3.ui.screens.post

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.feedm3.R
import com.example.feedm3.data.Post

@Composable
fun PostItem(
    modifier: Modifier = Modifier,
    post: Post
) {
    Card(modifier = modifier.heightIn(268.dp)) {
        ConstraintLayout(modifier = Modifier.fillMaxWidth()) {
            val (
                header,
                excerpt,
                author,
                title,
                date
            ) = createRefs()

            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(140.dp)
                    .constrainAs(header) {
                        start.linkTo(parent.start)
                        top.linkTo(parent.top)
                    },
                model = ImageRequest.Builder(LocalContext.current)
                    .data(post.image)
                    .crossfade(true)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .build(),
                contentDescription = null,
                contentScale = ContentScale.Crop
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.Black.copy(alpha = 0.4f))
                    .padding(horizontal = 12.dp, vertical = 8.dp)
                    .constrainAs(title) {
                        bottom.linkTo(header.bottom)
                    },
                text = post.title,
                textAlign = TextAlign.Start,
                color = Color.White
            )

            Text(
                modifier = Modifier
                    .constrainAs(excerpt) {
                        top.linkTo(header.bottom, margin = 12.dp)
                        start.linkTo(title.start)
                        end.linkTo(title.end)
                    }
                    .padding(end = 16.dp),
                text = post.excerpt,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )

            Text(
                modifier = Modifier
                    .padding(12.dp)
                    .constrainAs(date) {
                        end.linkTo(parent.end)
                        bottom.linkTo(parent.bottom)
                    },
                text = post.publishDate,
                fontSize = 14.sp
            )

            Text(
                modifier = Modifier
                    .padding(12.dp)
                    .constrainAs(author) {
                        bottom.linkTo(parent.bottom)
                        end.linkTo(date.start)
                    },
                text = post.author
            )

            createHorizontalChain(
                author,
                date,
                chainStyle = ChainStyle.SpreadInside
            )
        }
    }
}